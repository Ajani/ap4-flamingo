-module(test_drop).
-import(flamingo,[]).
-include_lib("eunit/include/eunit.hrl").

hello(_,_,_) -> {no_change, "hello"}.
bye(_,_,_) -> {no_change, "bye"}.


drop_test() ->
    {ok,Server} = flamingo:new("Server"),
    {ok,Id1} = flamingo:route(Server, ["/hello"], fun hello/3, none),
    {ok,_} = flamingo:route(Server, ["/bye"], fun bye/3, none),
    Me = self(),
    Ref = make_ref(),
    Ref2 = make_ref(),
    Ref3 = make_ref(),
    flamingo:request(Server, {"/hello", []},
                     Me, Ref),
    receive
        {Ref, Reply} -> ?assert(Reply =:= {200,"hello"})
    end,    

    flamingo:request(Server, {"/bye", []},
                     Me, Ref2),
    receive
        {Ref2, Reply2} -> ?assert(Reply2 =:= {200,"bye"})
    end,

    flamingo:drop_group(Server,Id1),

    flamingo:request(Server, {"/hello", []},
                     Me, Ref3),
    receive
        {Ref3,{Code, _}} -> ? assert(Code =:= 404)
    end,
    Ref4 = make_ref(),
    flamingo:request(Server, {"/bye", []},
                     Me, Ref4),
    receive
        {Ref4, Reply4} -> ?assert(Reply4 =:= {200,"bye"})
    end.

hello_test() ->
    S = hello:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S,{"/hello",[]},Me,Ref),
    receive
        {Ref,Reply} -> ?assert(Reply =:= {200,"Hello my friend"})
    end,
    Ref2 = make_ref(),
    flamingo:request(S,{"/goodbye",[]},Me,Ref2),
    receive
        {Ref2,Reply2} -> ?assert(Reply2 =:= {200,"Sad to see you go."})
    end.

bad_route_test() -> 
    S = hello:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S,{"/whops",[]},Me,Ref),
    receive
        {Ref,{Code,_}} -> ?assert(Code =:= 404)
    end.

sad_mood_test() ->
    S = mood:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S,{"/mood",[]},Me,Ref),
    receive
        {Ref,Reply} -> ?assert(Reply =:= {200,"Sad"})
    end.

happy_mood_test() ->
    S = mood:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S,{"/moo",[]},Me,Ref),
    receive
        {Ref,Reply} -> ?assert(Reply =:= {200,"That's funny"})
    end,
    Ref2 = make_ref(),
    flamingo:request(S, {"/mood", []}, Me, Ref2),
    receive
        {Ref2,Reply2} -> ?assert(Reply2 =:= {200, "Happy!"})
    end.

first_count_to_one_counter_test()->
    S = counter:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S, {"/inc_with", []},
                     Me, Ref),
    receive
        {Ref, Result} -> ?assert(Result =:= {200, "1"})
    end.    
second_count_to_one_counter_test()->
    S = counter:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S, {"/inc_with", [{"x", "1"}]},
                     Me, Ref),
    receive
        {Ref, Result} -> ?assert(Result =:= {200, "1"})
    end.

third_count_to_one_counter_test()->
    S = counter:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S, {"/inc_with", [{"x", "asbas"}]},
                     Me, Ref),
    receive
        {Ref, Result} -> ?assert(Result =:= {200, "1"})
    end.

fourth_count_to_one_counter_test()->
    S = counter:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S, {"/inc_with", [{"y", "1"}]},
                     Me, Ref),
    receive
        {Ref, Result} -> ?assert(Result =:= {200, "1"})
    end.

fifth_count_to_one_counter_test()->
    S = counter:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S, {"/inc_with", [{"y", "12"},{"x","1"}]},
                     Me, Ref),
    receive
        {Ref, Result} -> ?assert(Result =:= {200, "1"})
    end.