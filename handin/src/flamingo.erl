-module(flamingo).
-import(route_map,[]).
-export([new/1, request/4, route/4, drop_group/2, get/2]).

new(_Global) ->
   D = route_map:new(),
   {ok, spawn(fun()->loop(_Global,D) end)}.

request(_Flamingo, _Request, _From, _Ref) -> _Flamingo ! {request,_Request, _From, _Ref}.

route(_Flamingo, _Path, _Fun, _Arg) ->
  Ref = make_ref(),
  case _Path of
    [] -> {error,no_path_given};
    _ -> _Flamingo ! {add, Ref, _Path, _Fun, _Arg},
      {ok,Ref} 
  end.
  
drop_group(_Flamingo, _Id) -> _Flamingo ! {drop, _Id}.

get(_Flamingo, _From) -> _Flamingo ! {get, _From}.

route_loop(State) -> 
  receive
    exit -> skip
  after 0 ->
    receive
      {Fun, {Path, Args}, From, _Ref} ->
        try Fun({Path, Args}, From, State) of
          {new_state, Content, NewState} -> 
            From ! {_Ref, {200, Content}},
            route_loop(NewState);
          {no_change, Content} -> From ! {_Ref, {200, Content}},
            route_loop(State)
        catch
          _:_ -> From ! {_Ref, {500, server_error}},
            route_loop(State)
        end
      end
    end.

loop(_Global, Flamingo) ->
  receive
    {drop, _Id} -> 
      case route_map:deleteRouteByID(Flamingo,_Id) of
        {OL, false} -> loop(_Global ,OL);
        {NL,PID} -> 
          PID ! {exit},
          loop(_Global,NL)
      end 
  after 0 ->
    receive
      {get, From} ->
        From ! {Flamingo};
      {add, Ref,_Path, _Fun, _Arg} ->
        NF = route_map:addRoute(Flamingo,Ref,_Path, _Fun, _Arg, fun route_loop/1),
        loop(_Global,NF);
      {request, {Path, Args}, From, _Ref} ->
          case route_map:findElementW_Prefix(Flamingo, Path) of
            false -> From ! {_Ref, {404, path_not_registered}}, loop(_Global,Flamingo);
            {PF,{_,_,Fun,PID}} ->
              try PID ! {Fun, {PF, Args}, From, _Ref} of
                _ -> loop(_Global, Flamingo)         
              catch 
                _:_ -> From ! {_Ref, {404, path_not_registered}},
                loop(_Global, Flamingo)
              end 
          end
        end
  end.