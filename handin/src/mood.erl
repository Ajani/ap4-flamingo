-module(mood).
-export([server/0,try_it/1,try_it2/1]).

server() ->
    {ok, F} = flamingo:new("The Flamingo Server"),
    flamingo:route(F, ["/moo", "/mood"], fun mood/3, false),
    F.

mood({_Path, _}, _, State) ->
  if
    _Path =:= "/moo" ->
      {new_state, "That's funny", true};
    _Path =:= "/mood" ->
      case State of 
        false -> {no_change, "Sad"};
        true -> {no_change, "Happy!"}
    end
  end.


try_it(Server) ->
    Me = self(),
    Ref = make_ref(),
    % flamingo:get(Server,Me),
    flamingo:request(Server, {"/moo", []},
                      Me, Ref),
    receive
        {Ref, Reply} -> Reply
      % Reply -> Reply
  end.


try_it2(Server) ->
    Me = self(),
    Ref = make_ref(),
    % flamingo:get(Server,Me),
    flamingo:request(Server, {"/mood", []},
                      Me, Ref),
    receive
        {Ref, Reply} -> Reply
      % Reply -> Reply
  end.