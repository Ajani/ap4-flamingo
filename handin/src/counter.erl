-module(counter).
-export([server/0,try_it/0]).

server() ->
    {ok, F} = flamingo:new("The Flamingo Server"),
    flamingo:route(F, ["/inc_with", "/dec_with"], fun math/3, 0),
    F.

math({_Path, _Args}, _, State) ->
  if
    _Path =:= "/inc_with" ->
      NewVal = State + get_arg_value(_Args),
      {new_state, integer_to_list(NewVal), NewVal};
    _Path =:= "/dec_with" ->
      NewVal = State - get_arg_value(_Args),
      {new_state, integer_to_list(NewVal), NewVal}
  end.

get_arg_value(_Args) ->
    case _Args of
        [] -> 1;
        [{"x",Val}|_] ->
            try list_to_integer(Val) of
                Number ->
                    if 
                        Number < 0 -> 1;
                        true -> Number
                    end
            catch
                _:_ -> 1
            end;
        [_|T] -> get_arg_value(T)
    end. 

try_it() ->
    Server = server(),
    Me = self(),
    Ref = make_ref(),
    Ref2 = make_ref(),
    Ref3 = make_ref(),
    flamingo:request(Server, {"/inc_with", [{"x", "2"}]},
                     Me, Ref),
    receive
        {Ref, _} -> skip
    end,    

    flamingo:request(Server, {"/inc_with", [{"x", "2"}]},
                     Me, Ref2),
    receive
        {Ref2, _} -> skip
    end,

    flamingo:request(Server, {"/inc_with", [{"x", "2"}]},
                     Me, Ref3),
    receive
        {Ref3,_} -> skip
    end,
    Ref4 = make_ref(),
    flamingo:request(Server, {"/inc_with", [{"x", "2"}]},
                     Me, Ref4),
    receive
        {Ref4,Rep} -> Rep
    end.

