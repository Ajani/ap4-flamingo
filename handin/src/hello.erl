-module(hello).
-export([server/0,try_it/1,try_unregister_multiple/0]).

server() ->
    {ok, F} = flamingo:new("The Flamingo Server"),
    flamingo:route(F, ["/hello", "/goodbye"], fun hello/3, none),
    F.

hello({_Path, _}, _, _) ->
  if
    _Path =:= "/hello" ->
      {no_change, "Hello my friend"};
    _Path =:= "/goodbye" ->
      {no_change, "Sad to see you go."};
    true ->error("Unreachable endpoint in hello module.")
  end.

try_it(Server) ->
    Me = self(),
    Ref = make_ref(),
    flamingo:route(Server, ["/hello", "/gid"], fun hello/3, none),
    flamingo:request(Server, {"/hellooop", []},
                      Me, Ref),
    receive
        {Ref, Reply} -> Reply
  end.

try_unregister_multiple()->
  {ok, S} = flamingo:new(""),
  Me = self(),
  {ok, GID} =
    flamingo:route(S, ["/hello", "/goodbye"], fun hello/3, none),
  Requests = lists:seq(0,1000),
  
  lists:foreach(fun(_)->
    flamingo:request(S, {"/hello", []}, Me, none) end,
  Requests),
  flamingo:drop_group(S, GID),
  lists:foreach(fun(_) ->
    receive
    {none, Reply} -> io:fwrite("~w~n", [Reply])
    end
  end, Requests).