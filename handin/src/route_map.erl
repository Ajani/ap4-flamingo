-module(route_map).
-export([new/0,addRoute/6,findElement/2,deleteRouteByID/2,findElementW_Prefix/2]).

new() -> [].

deleteRouteByID(RouteMap, _Id) ->
    case findElementById(RouteMap, _Id) of
        {Ref,_PathList,Fun,PID} -> 
            NL = lists:delete({Ref,_PathList,Fun,PID},RouteMap),
            {NL,PID};
        false -> {RouteMap,false}
    end.

findElementById(RouteMap,_Id) -> 
    case RouteMap of
        [] -> false;
        [{Ref,_PathList,Fun,PID} | T] ->
            if 
                Ref =:= _Id -> {Ref,_PathList,Fun,PID};
                true -> findElementById(T,_Id)
            end
    end.

addRoute(RouteMap, Ref, _PathList, Fun, IEnv, Loop) ->
    case RouteMap of
        [] -> [{Ref,_PathList, Fun, spawn(fun()-> Loop(IEnv) end)}];
        RouteMap -> process_duplicates(RouteMap,_PathList, Ref, _PathList, Fun, IEnv, Loop)
    end.

process_duplicates(RouteMap, Paths_to_check, Ref, _PathList, Fun, IEnv, Loop) ->
    case Paths_to_check of
        [] -> lists:append(RouteMap,[{Ref,_PathList,Fun, spawn(fun()-> Loop(IEnv) end)}]);
        [H|T] -> 
            case findElement(RouteMap,H) of
                false -> lists:append(RouteMap,[{Ref,_PathList,Fun, spawn(fun()-> Loop(IEnv) end)}]);
                {_Ref,_P,_Fun,PID} ->
                    case lists:delete(H,_P) of 
                        [] -> 
                            NL = lists:delete({_Ref,_P,_Fun,PID},RouteMap),
                            PID ! {exit},
                            process_duplicates(NL, T, Ref, _PathList, Fun, IEnv, Loop);
                        [H2|T2] -> 
                            NLD = lists:delete({_Ref,_P,_Fun,PID},RouteMap),
                            NL = lists:append(NLD,[{Ref,[H2|T2],Fun,PID}]),
                            process_duplicates(NL, T, Ref, _PathList, Fun, IEnv, Loop)
                    end
            end
    end.

findElementW_Prefix(RouteMap, _Path) ->
    case findElementW_Prefix(RouteMap, _Path, "") of 
        "" -> false;
        Prefix -> {Prefix, findElement(RouteMap, Prefix)}
    end.

findElementW_Prefix(RouteMap, _Path, Longest_prefix) ->
    case RouteMap of 
        [] -> Longest_prefix;
        [H|T] -> 
            case findElementW_Prefix_internal(_Path,H,Longest_prefix) of
                Longest_prefix -> findElementW_Prefix(T,_Path,Longest_prefix);
                Something_Else -> findElementW_Prefix(T,_Path,Something_Else)
            end
    end.

findElementW_Prefix_internal(_Path,{_,_PathList,_,_},Longest_prefix) -> 
    findElementW_Prefix_in_list(_PathList, _Path ,Longest_prefix).    

findElementW_Prefix_in_list(List, _Path,Longest_prefix) -> 
    case List of 
        [] -> Longest_prefix;
        [H|T] ->
            Prefix_length = string:length(H),
            Path_length = string:length(_Path), 
            Longest_prefix_length = string:length(Longest_prefix),
            if 
                Longest_prefix_length > Prefix_length -> findElementW_Prefix_in_list(T, _Path,Longest_prefix);
                Prefix_length > Path_length -> findElementW_Prefix_in_list(T, _Path,Longest_prefix);
                Prefix_length =:= Path_length -> 
                    if 
                        H =:= _Path -> H;
                        H /= _Path -> findElementW_Prefix_in_list(T, _Path,Longest_prefix)
                    end;
                Prefix_length < Path_length -> 
                    Path_Substring = string:slice(_Path,0,Prefix_length), 
                    if 
                        Path_Substring =:= H -> findElementW_Prefix_in_list(T, _Path,H);
                        true -> findElementW_Prefix_in_list(T, _Path,Longest_prefix)
                    end
            end
    end.
    

findElement(RouteMap, _Path) ->
    case RouteMap of 
        [] -> false;
        [H|T] -> 
            case findElement_internal(_Path,H) of
                false -> findElement(T,_Path);
                {Ref,_P, Fun, PID} -> {Ref,_P,Fun,PID}
            end
    end.

findElement_internal(_Path,{Ref,_PathList,Fun,PID}) ->
    case lists:member(_Path,_PathList) of 
        true -> {Ref,_PathList,Fun,PID};
        false -> false
    end.
