-module(test).
-import('../src/hello',[server/0]).
-compiler([export_all]).
-include_lib("eunit/include/eunit.hrl").

hello_test() -> 
    S = hello:server(),
    Me = self(),
    Ref = make_ref(),
    flamingo:request(S,{"/hello",[]},Me, Ref),
    receive 
        {Ref,Reply} -> ?assert(Reply =:= "Hello my friend")
    end.
